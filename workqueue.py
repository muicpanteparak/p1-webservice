import env
from logger import Logger
import rabbitmq
from flask import Flask
from flask import request
from flask_cors import CORS
import json
from api import job

LOG = Logger("workqueue")

QUEUE_HOST = env.load_env("QUEUE_HOST")
QUEUE_PORT = env.load_env("QUEUE_PORT")

HTTP_STATUS_OK = 200
HTTP_STATUS_BAD_REQUEST = 400
HTTP_STATUS_NOT_FOUND = 404
rabbit_connector = rabbitmq.init()

app = Flask(__name__)
CORS(app)


@app.route('/api/workqueue/job', methods=['POST'])
def add_job_workqueue():
    content = request.get_json(silent=True)
    bucket_name, object_name = content["bucket_name"], content["object_name"]
    
    job.delete_file(bucket_name, object_name + ".gif")
    job.submit_job(bucket_name, object_name, object_name + ".gif")
    return json.dumps(content)


@app.route('/api/workqueue/job/all', methods=["POST"])
def execute_all_jobs():
    content = request.get_json(silent=True)
    bucket_name = content["bucket_name"]
    # bucketlst = [x["name"] for x in job.files(bucket_name)]
    lst = []

    for obj in job.files(bucket_name):
        name = obj
        
        # if not name.endswith(".gif"):
        print(bucket_name, name["name"])
        job.delete_file(bucket_name, name["name"] + ".gif")
        lst.append(name["name"])
        job.submit_job(bucket_name, name["name"], name["name"] + ".gif")
    return json.dumps(lst)


def no_gif(lst):
    gif = []
    for ob in lst:
        if ob.endswith(".gif"):
            gif.append(ob[:-4])

    # gif = [ob["name"][:-4] for ob in lst if ob["name"].endwith(".gif") ]

    nogif = []
    for ob in lst:
        if not ob.endswith(".gif"):
            nogif.append(ob)

    for g in gif:
        nogif.remove(g)

    return nogif


def gif(lst):
    giff = []
    for ob in lst:
        if ob.endswith(".gif"):
            giff.append(ob)

    # gif = [ob["name"][:-4] for ob in lst if ob["name"].endwith(".gif") ]
    # nogif = []
    # for ob in lst:
    #     if not ob.endswith(".gif"):
    #         nogif.append(ob)

    # for g in nogif:
    #     giff.remove(g)

    return giff


@app.route('/api/<bucketname>/show', methods=["GET"])
def list_gif(bucketname):
    data = job.files(bucketname)

    if data == None:
        return json.dumps({}), 400
    return json.dumps(data)

@app.route('/api/remove', methods=["POST"])
def remove_one():
    content = request.get_json(silent=True)
    bucket_name = content["bucket_name"]
    gif = content["object_name"]
    job.delete_file(bucket_name, gif)
    return json.dumps([gif])

@app.route('/api/remove/all', methods=["POST"])
def remove_all():
    content = request.get_json(silent=True)
    bucket_name = content["bucket_name"]
    gifs = ([x["name"] for x in job.files(bucket_name)])
    
    for g in gif(gifs):
        job.delete_file(bucket_name, g)


    return json.dumps(gifs)


def main():
    app.run('0.0.0.0', debug=False, threaded=True)

if __name__ == '__main__':
    try:
        main()
    except Exception as e:
        LOG.debug(msg=str(e))
    # print(did_gen_gif(["aaa", "vbbb", "aaa.gif"]))
    # job.submit_job("abcd", "ababa121", "ababa121" + ".gif")
