import logging


class Logger(logging.getLoggerClass()):
    def __init__(self, name):
        # set up logging to file - see previous section for more details
        logging.basicConfig(level=logging.INFO,
                            format='%(asctime)s %(name)-50s %(levelname).8s %(message)s',
                            datefmt='%d-%m-%y %H:%M:%S')
        self.logging = logging.getLogger(name)

    def warning(self, msg, *args, **kwargs):
        self.logging.warning(msg, *args, **kwargs)

    def info(self, msg, *args, **kwargs):
        self.logging.info(msg, *args, **kwargs)

    def error(self, msg, *args, **kwargs):
        self.logging.error(msg, *args, **kwargs)

    def debug(self, msg, *args, **kwargs):
        self.logging.debug(msg, *args, **kwargs)
