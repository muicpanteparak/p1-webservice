import requests
import env
import json
import logger

LOG = logger.Logger("storage")

BASE_URL = env.load_env("STORAGE_URL")

def remove_object(bucket_name, object_name):
    with requests.Session() as s:
        resp = s.delete(f"{BASE_URL}/{bucket_name}/{object_name}?delete")

        return None


def list_objects(bucket_name):
    # req = requests.get(f"{BASE_URL}/{bucket_name}?list")
    #
    # if req.status_code != 200:
    #     req.close()
    #     return None
    # obj = json.loads(req.content)
    #
    # return obj["objects"]

    with requests.Session() as s:
        resp = s.get(f"{BASE_URL}/{bucket_name}?list")
        if resp.status_code != 200:
            LOG.info(f"Status Failed: {resp.status_code}, list /{bucket_name}")
            raise RuntimeError("List failed")

        return json.loads(resp.content)['objects']

# [{'name': 'ababab', 'created': '2018-10-04T19:27:32', 'modified': '2018-10-04T19:27:33', 'etag': '53824bad0ecd21c3cc3095bb05273ffd-1'}]
