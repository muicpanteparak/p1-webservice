import rabbitmq
import json
from logger import Logger
from api.storage import list_objects, remove_object
Worker = rabbitmq.init()
QUEUE_NAME = "video"
LOG = Logger("job")


def submit_job(bucket_name, object_name, gif_name):
    LOG.info(msg=f"submit job {bucket_name}/{object_name} -> {gif_name}")
    d = {
        "bucket_name": bucket_name,
        "object_name": object_name,
        "gif_name": gif_name
    }

    global Worker
    if Worker is None or Worker.is_closed():
        LOG.info("Connection closed")
        Worker = rabbitmq.init()
    Worker.publish(QUEUE_NAME, json.dumps(d))


def files(bucket_name: str):
    return list_objects(bucket_name)


def delete_file(bucket_name, object_name):
    return remove_object(bucket_name, object_name)