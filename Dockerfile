FROM python:3.7-alpine

WORKDIR /app
ADD https://raw.githubusercontent.com/eficode/wait-for/master/wait-for .
RUN chmod +x wait-for
ADD requirements.txt requirements.txt
RUN pip install -r requirements.txt

ADD . .

CMD ["./wait-for", "rabbitmq:5672", "-t", "300", "--", "./wait-for", "storage-service:8080","-t","30", "--", "python", "-u", "workqueue.py"]
