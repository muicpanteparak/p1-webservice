"""
env initializer
"""


import os


def load_env(key: str):
    """
    Load Env from docker if exist
    """
    queue_host = os.getenv("QUEUE_HOST", "127.0.0.1")
    queue_port = os.getenv("QUEUE_PORT", 5672)
    queue_username = os.getenv("QUEUE_USERNAME", "guest")
    queue_password = os.getenv("QUEUE_PASSWORD", "guest")
    storage_url = os.getenv("STORAGE_URL", "http://localhost:8080")

    return {
        "QUEUE_HOST": queue_host,
        "QUEUE_PORT": queue_port,
        "QUEUE_USERNAME": queue_username,
        "QUEUE_PASSWORD": queue_password,
        "STORAGE_URL": storage_url
    }[key]
