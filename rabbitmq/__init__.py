import pika
from logger import Logger
import env
import time

LOG = Logger("rabbitmq")

inst = None

QUEUE_HOST = env.load_env("QUEUE_HOST")
QUEUE_PORT = env.load_env("QUEUE_PORT")
QUEUE_USER = env.load_env("QUEUE_USERNAME")
QUEUE_PASS = env.load_env("QUEUE_PASSWORD")
STORAGE_URL = env.load_env("STORAGE_URL")


class Connector(object):
    def __init__(self, host: str = "localhost", port: int = 5672, username: str = "user", password: str = "password"):
        LOG.debug(msg=f"Connecting to {host}:{port}")

        self.host = host
        self.port = port
        self.username = username
        self.password = password

        self.connection = pika.BlockingConnection(
            pika.ConnectionParameters(
                host=host,
                port=port,
                credentials=pika.PlainCredentials(
                    username=username,
                    password=password
                )
            )
        )

    def reconnect(self):
        self.connection = pika.BlockingConnection(
            pika.ConnectionParameters(
                host=self.host,
                port=self.port,
                credentials=pika.PlainCredentials(
                    username=self.username,
                    password=self.password
                )
            )
        )

    def disconnect(self):
        LOG.debug(msg=f"Disconnecting...")
        self.connection.close()

    def is_closed(self):
        return self.connection.is_closed

    def get_channel(self):
        retry = 0
        while retry < 5:
            try:
                channel = self.connection.channel()
                return channel
            except pika.exceptions.ConnectionClosed:
                LOG.info('oops. lost connection. trying to reconnect.')
                time.sleep(0.5)
                self.reconnect()
                retry += 1

        if retry >= 5:
            LOG.info("Maximum reconnect exceeded")

    def consume(self, callback, channel_name: str):
        LOG.debug(msg="Consuming Event")
        channel = self.get_channel()
        self.declare(channel_name, channel)
        channel.basic_consume(callback, queue=channel_name, no_ack=True)
        return channel

    def start_consuming(self, channel):
        LOG.info(msg="Start Blocking Consumer")
        channel.start_consuming()

    def publish(self, channel_name: str, message: str):
        LOG.debug(msg=f"Publishing to {channel_name} - {message}")
        channel = self.get_channel()
        self.declare(channel_name, channel)
        channel.basic_publish(exchange='',
                              routing_key=channel_name,
                              body=message)
        channel.close()

    def declare(self, name, channel):
        channel.queue_declare(
            queue=name, durable=True,
            exclusive=False, auto_delete=False
        )


def init():
    global inst
    if inst is None:
        LOG.debug(msg='Starting Rabbit MQ Connection')
        inst = Connector(host=QUEUE_HOST, port=QUEUE_PORT, username=QUEUE_USER, password=QUEUE_PASS)

    return inst


def disconnect():
    global inst
    if inst is not None:
        inst.disconnect()
        inst = None


if __name__ == "__main__":
    con = Connector()
    con.publish("video", '{"bucket_name": "abc", "object_name": "hello", "gif_name": "thisgif"}')
    con.disconnect()
